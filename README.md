# Компьютерное зрение
Инструкция установки
---
`python3 -m venv cv`

`pip3 install opencv-python, numpy, notebook, keras`

`pip install --upgrade imutils`

`python -m pip install -U scikit-image`

`pip3 install --upgrade tensorflow`

Описание проекта представлено на этой [странице](https://gitlab.com/konstantin_kon/computer_vision/-/wikis/home)
