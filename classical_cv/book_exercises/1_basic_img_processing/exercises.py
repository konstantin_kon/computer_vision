import cv2
import numpy as np

def unsharp_mask(img, color=True):
    '''
    операция нерезкого маскирования
    '''
    if color:
        blur = cv2.GaussianBlur(img, (5,5), 0)
        #result = -0.5*blur + 1.5*img
        result = cv2.addWeighted(img, 1.5, blur, -0.5, 0)
        return result
    else:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5,5), 0)
        result = cv2.addWeighted(gray, 1.5, blur, -0.5, 0)
        return result

def norm_image(img):
    blur = cv2.GaussianBlur(img, (5,5), 0)
    result = img/blur
    return result

if __name__ == '__main__':
    img = cv2.imread('img/dog_bike.jpg')
    #res = unsharp_mask(img, False)
    res = norm_image(img)
    cv2.imshow('Original', img)
    cv2.imshow('Image_result', res)
    cv2.waitKey(0)
    cv2.destroyAllWindows()