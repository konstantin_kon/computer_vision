import cv2
import numpy as np

img = cv2.imread('img/dog_bike.jpg')
rows, cols, h = img.shape

pts1 = np.float32([[100, 150], [300, 150], [100, 250]])
pts2 = np.float32([[20, 150],[350, 150], [100, 200]])

for pt in pts1:
    cv2.circle(img, tuple(pt), 3, (0,255,0),-1)

M = cv2.getAffineTransform(pts1, pts2)

res = cv2.warpAffine(img, M, (cols, rows))

cv2.imshow('Image', img)
cv2.imshow('Image result', res)
cv2.waitKey(0)
cv2.destroyAllWindows()