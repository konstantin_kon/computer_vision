from scipy.ndimage import filters
import numpy as np
import matplotlib.pyplot as plt 
from PIL import Image

def compute_harris_response(img, sigma=3):
    '''
    вычисление отклика детектора углов Харриса
    '''
    # производные
    imx = np.zeros(img.shape)
    filters.gaussian_filter(img,(sigma,sigma), (0,1), imx)
    imy = np.zeros(img.shape)
    filters.gaussian_filter(img,(sigma,sigma), (1,0), imy)

    # элементы матрицы Харриса
    Wxx = filters.gaussian_filter(imx*imx, sigma)
    Wxy = filters.gaussian_filter(imx*imy, sigma)
    Wyy = filters.gaussian_filter(imy*imy, sigma)

    # определитель и след матрицы
    Wdet = Wxx*Wyy - Wxy**2
    Wtr = Wxx + Wyy

    return Wdet / Wtr

def get_harris_points(harrisim, min_dist=10, threshold=0.1):
    '''
    Возвращает углы на изо, построенном по функции отклика Харриса
    min_dist - минимальное число пикселей между углами и границей
    изо
    '''

    # найти кандидаты, для которых ф отклика больше порога
    corner_threshold = harrisim.max() * threshold
    harrisim_t = (harrisim > corner_threshold)*1

    # вычислить координаты кандидатов
    coords = np.array(harrisim_t.nonzero()).T

    # вычислить значения кандидатов
    candidate_value = [harrisim[c[0], c[1]] for c in coords]

    # отсортировать их
    index = np.argsort(candidate_value)

    # сохранить в массив
    allowed_locations = np.zeros(harrisim.shape)
    allowed_locations[min_dist:-min_dist, min_dist:-min_dist] = 1

    # выбрть наилучшие с учётом минимальной дистанции
    filtered_coords = []
    for i in index:
        if allowed_locations[coords[i,0],coords[i,1]] == 1:
            filtered_coords.append(coords[i])
            allowed_locations[(coords[i,0]-min_dist):(coords[i,0]+min_dist),
            (coords[i,1]-min_dist):(coords[i,1]+min_dist)] = 0

    return filtered_coords

def plot_harris_points(img, filtered_coords):
    plt.figure()
    plt.gray()
    plt.imshow(img)
    plt.plot([p[1] for p in filtered_coords], [p[0] for p in filtered_coords], '*')
    plt.axis('off')
    #plt.show()

img = np.array(Image.open('img/dog_bike.jpg').convert('L'))
harrism = compute_harris_response(img)
filtered_coords = get_harris_points(harrism, 6)
plot_harris_points(img, filtered_coords)
# отклик Харриса
plt.figure('Figure 2')
plt.gray()
plt.imshow(harrism)
plt.show()