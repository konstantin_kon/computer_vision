import cv2
import matplotlib.pyplot as plt

img = cv2.imread('img/dog_bike.jpg',0)
img1 = cv2.GaussianBlur(img, (7,7), 0)
img2 = cv2.GaussianBlur(img, (3,3), 0)

hist = cv2.calcHist([img],[0],None,[256],[0, 256])

res = img1 - img2
sob = cv2.Sobel(img, cv2.CV_64F,1,0,ksize=5)

cv2.imshow('Result image', sob)
cv2.imshow('Original image', img)

plt.subplot(1,2,1)
plt.plot(sob)
plt.subplot(1,2,2)
plt.hist(sob.ravel(),256,[0,256])
plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()