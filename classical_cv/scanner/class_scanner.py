import cv2
import numpy as np

class Scanner:

    def find_contours(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        edges = cv2.Canny(gray, 180, 200)
        contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        cnt = []
        for contour in contours:
            if cv2.contourArea(contour) < 500:
                continue
            cnt.append(contour)
            cv2.drawContours(img,[contour],0, (0,0,255))
        return img, cnt

    def affin_transform(self, img, cnt):
        rows, cols, _ = img.shape    
        pts1 = np.float32([cnt[0][0], cnt[0][20], cnt[0][-20]])
        cv2.circle(img,(pts1[0,0][0],pts1[0,0][1]),5,(0,0,255)) # red
        cv2.circle(img,(pts1[1,0][0],pts1[1,0][1]),5,(0,255,0)) # green
        cv2.circle(img,(pts1[2,0][0],pts1[2,0][1]),5,(255,0,0)) # blue
        print(pts1[0,0][0], pts1[0,0][1])
        print(pts1[1,0][0],pts1[1,0][1])
        pts2 = np.float32([[266,109], [246,109], [266,126] ])
        print('pts1 = ', pts1, '\npts2 = ', pts2)
        M = cv2.getAffineTransform(pts1, pts2)
        res = cv2.warpAffine(img, M, (cols, rows))
        return res, img

    def perspectiv_transform(self, img, box):
        #rows, cols, _ = img.shape
        new_box = self.rotate_rect(box)
        pts1 = np.float32([box])
        pts2 = np.float32([new_box])
        M = cv2.getPerspectiveTransform(pts1, pts2)
        res= cv2.warpPerspective(img, M, (0, 0))
        new_box = np.int16([new_box])
        #print(new_box)
        image = res[new_box[0,1,1]:new_box[0,0,1], new_box[0,1,0]:new_box[0,2,0]]
        return image, img

    def rotate_rect(self, box):
        """
        произвольно ориентированный четырёхугольник 
        поворачивает прямо и вычисляет размеры
        """
        res = np.zeros((4,2))
        res[0] = [box[1][0], box[3][1]]
        res[1] = [box[1][0], box[2][1]]
        res[2] = box[2]
        res[3] = [box[2][0], box[3][1]]
        size = (int(res[1][0]-res[0][0]), int(res[2][1]-res[1][1]))
        #print(size)
        #print(res)
        return res
        

    def get_corner(self, img, cnts):
        '''
        выделяет углы из контуров
        '''
        for cnt in cnts:
            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            #print(box)
            cv2.drawContours(img, [box],0,(0,255,0))
            cv2.imshow('drawContours', img)
        return box
    
    def scan(self, img):
        '''
        главная функция, которая выполняет все преобразования
        '''
        edges, contour = self.find_contours(img)
        box = self.get_corner(img, contour)
        res, image = self.perspectiv_transform(img, box)
        return res, image

if __name__ == '__main__':
    img = cv2.imread('img/scanner.jpg')
    img = cv2.resize(img, (int(img.shape[1]/2),int(img.shape[0]/2)))
    S = Scanner()
    res, image = S.scan(img)
    cv2.imshow('Image', image)
    cv2.imshow('Image_result', res)
    cv2.waitKey(0)
    cv2.destroyAllWindows()