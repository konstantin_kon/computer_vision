import cv2
import class_scanner as scanner
import scan

cam = cv2.VideoCapture(0)
Scan = scanner.Scanner()
version = input('Какой вариант реализации?\n')
if version == 'my':
    print('Мой вариант')
else:
    print('Реализация Adrian Rosebrock из pyimagesearch')
    
try:
    while True:
        _, frame = cam.read()
        cv2.imshow('Video',frame)
        
        if cv2.waitKey(1) == ord('s'):
            if version == 'my':
                res, image = Scan.scan(frame)
                cv2.imshow('Res',res)
            else:
                scan.main(frame)

        elif cv2.waitKey(1) == 27:
            break
#except:
#    pass
#except Exception as e:
#    print(e)
finally:
    cv2.destroyAllWindows()
    cam.release()
