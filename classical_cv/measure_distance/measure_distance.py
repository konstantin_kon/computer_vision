import cv2
import numpy as np

img = cv2.imread('img/objects2.jpg')

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
canny = cv2.Canny(gray,50,100)
ret, thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)

contour, hierarcy = cv2.findContours(canny,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

contrs = [150, 300]

cv2.drawContours(img, contour, contrs[0], (0,0,255),1)
cv2.drawContours(img, contour, contrs[1], (0,0,255),1)

cx, cy = [], []
for contr in contrs:
    cnt = contour[contr]
    M = cv2.moments(cnt)
    cx.append(int(M['m10'] / M['m00']))
    cy.append(int(M['m01'] / M['m00']))
print('cx = ',cx, 'cy = ',cy)
dist = np.sqrt((cx[1]-cx[0])**2 + (cy[1]-cy[0])**2)
print(dist)

cv2.putText(img,str(int(dist)), (cx[0]+10,cy[0]), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 1, (255,0,0))
cv2.line(img,(cx[0],cy[0]), (cx[1],cy[1]), (255,0,0),2)

cv2.imshow("Image",img)
#cv2.imshow("threshold",canny)

k = cv2.waitKey(0)
if k == 27:
    cv2.destroyAllWindows()