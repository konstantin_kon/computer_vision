import cv2

cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray,(5,5),5)
    canny = cv2.Canny(blur,50,100)
    contours,_ = cv2.findContours(canny,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        if cv2.contourArea(contour) < 500:
            continue
        cv2.drawContours(frame, contours, 1, (0,0,255),2)
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(frame, (x,y), (x+w,y+h),(255,0,0),2)
        cv2.circle(frame, (int(x+w/2),int(y+h/2)),2,(0,255,0),-1)
        M = cv2.moments(contour)
        if M['m00'] == 0:
            continue
        else:
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])

    cv2.imshow('Image',frame)
    #cv2.imshow('Mask',canny)

    k = cv2.waitKey(1)
    if k == 27:
        break

cv2.destroyAllWindows()
cap.release()
