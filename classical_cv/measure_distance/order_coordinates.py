import cv2
import numpy as np
import time
from scipy.spatial import distance as dist

img = cv2.imread('img/object3.jpg')

gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
blur = cv2.GaussianBlur(gray,(7,7),0)

canny = cv2.Canny(blur,50,100)
canny = cv2.dilate(canny, None, iterations=1)
canny = cv2.erode(canny, None, iterations=1)

cnts, _ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
coner_coor = []
for cnt in cnts:
    if cv2.contourArea(cnt) < 100:
        continue

    #x,y,w,h = cv2.boundingRect(cnt)
    #cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0))

    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    coner_coor.append(box)
    cv2.drawContours(img, [box], -1,(0,255,0))
'''
на изображении 3 объекта, найдём координаты 
углов этих объектов и соединим их линией
'''
first_ob = []
second_ob = []
third_ob = []
for i, l in enumerate(coner_coor):
    if i == 0:
        first_ob = l
    elif i == 1:
        second_ob = l
    elif i == 2:
        third_ob = l

for i in range(len(first_ob)):
    cv2.line(img, (first_ob[i,0],first_ob[i,1]),
    (second_ob[i,0],second_ob[i,1]),(0,0,0),2)
    d = dist.cdist([(first_ob[i])],[(second_ob[i])])
    di = np.round(d/27.222, 2)
    di = float(di)
    cv2.putText(img, '{:.1f}'.format(di),(first_ob[i,0]-10,first_ob[i,1]),
    cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255))
'''
размер визитки 9х5, размер в пикселях большей стороны 
равен 245. отсюда можно найти коэффициент, который будет вычислять 
расстояние в см. k = 245/9 = 27,2
'''
D = dist.cdist([(second_ob[1])],[(second_ob[2])])
print(D/27.2)
cv2.line(img, (second_ob[1,0],second_ob[1,1]), 
(second_ob[2,0],second_ob[2,1]), (0,0,255))


cv2.imshow('Image', img)
#cv2.imshow('Canny', canny)

if cv2.waitKey(0) == 27:
    cv2.destroyAllWindows()