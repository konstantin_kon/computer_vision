import cv2
import numpy as np
import time
from scipy.spatial import distance as dist

img = cv2.imread('img/object3.jpg')

gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
blur = cv2.GaussianBlur(gray,(7,7),0)

canny = cv2.Canny(blur,50,100)
canny = cv2.dilate(canny, None, iterations=1)
canny = cv2.erode(canny, None, iterations=1)

cnts, _ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
coner_coor = []
for cnt in cnts:
    if cv2.contourArea(cnt) < 100:
        continue

    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    coner_coor.append(box)
    cv2.drawContours(img, [box], -1,(0,255,0))
    for i in range(len(box)):
        if i < 3:
            D = dist.cdist([(box[i])], [(box[i+1])])
            D = D/27.22
            cx = int((box[i,0] + box[i+1,0])/2)
            cy = int((box[i,1] + box[i+1,1])/2)
        else:
            D = dist.cdist([(box[0])], [(box[3])])
            D = D/27.22
            cx = int((box[0,0] + box[3,0])/2)
            cy = int((box[0,1] + box[3,1])/2)
        D = float(D)        
        
        cv2.putText(img, '{:.2f}'.format(D),(cx,cy),
        cv2.FONT_HERSHEY_SIMPLEX,0.4,(0,0,0))
        #(box[0,0], box[0,1])

'''
размер визитки 9х5, размер в пикселях большей стороны 
равен 245. отсюда можно найти коэффициент, который будет вычислять 
расстояние в см. k = 245/9 = 27,2
'''


cv2.imshow('Image', img)
#cv2.imshow('Canny', canny)

if cv2.waitKey(0) == 27:
    cv2.destroyAllWindows()