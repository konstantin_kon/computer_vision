import cv2

cap = cv2.VideoCapture(0)

while True:
    ref, frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    _, frame1 = cap.read()
    frame1_gray = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)

    delta = cv2.absdiff(frame_gray, frame1_gray) 
    blur = cv2.GaussianBlur(delta, (5,5),0) 
    _, threshold = cv2.threshold(blur,10,255,cv2.THRESH_BINARY)
    contours,_ = cv2.findContours(threshold,cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        if cv2.contourArea(contour) < 1000:
            continue
        x,y,w,h = cv2.boundingRect(contour)
        frame = cv2.rectangle(frame, (x,y),(x+w,y+h),(0,0,255),2)
    cv2.imshow('Result - diff image', frame)

    if cv2.waitKey(1) == 27:
        break

cv2.destroyAllWindows()
cap.release()