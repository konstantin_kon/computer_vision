import cv2
"""
Мой алгоритм:
1) удаляем фон
2) находим контуры
3) рисуем прямоугольник
С веб камерой много ложных срабатываний. Так что этот способ не подходит

Обще принятый аглоритм:
1) переводим два изображения в оттенки серого
2) вычитаем одно из другого и детектируем движение
3) производим бинаризацию изображения
4) находим контуры
5) рисуем прямоугольники, если площадь контура меньше заданного, то не рисуем
С веб камерой, если медленно двигаться,то не замечает движения
"""
cap = cv2.VideoCapture('cv2/my_projects/img/vtest.avi')

# создадим объект, который позволит удалить фон
backSub = cv2.createBackgroundSubtractorKNN()

while True:
    ref, frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    _, frame1 = cap.read()
    frame1_gray = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)

    delta = cv2.absdiff(frame_gray, frame1_gray)  
    _, threshold = cv2.threshold(delta,10,255,cv2.THRESH_BINARY)
    contours1,_ = cv2.findContours(threshold,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours1:
        if cv2.contourArea(contour) < 1000:
            continue
        x,y,w,h = cv2.boundingRect(contour)
        frame1 = cv2.rectangle(frame1, (x,y),(x+w,y+h),(0,255,0),2)
    cv2.imshow('Result - diff image', frame1)

    # удалим фон и найдём контуры
    frame_back = backSub.apply(frame_gray)
    contours,_ = cv2.findContours(frame_back,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        if cv2.contourArea(contour) < 1000:
            continue
        x, y, w, h = cv2.boundingRect(contour)
        frame = cv2.rectangle(frame, (x,y), (x+w,y+h), (255,0,0), 2)

    cv2.imshow('Image',frame)
    if cv2.waitKey(1) == 27:
        break