import cv2
import numpy as np

'''
1) переводим в представление HSV 
2) выделяем необходимый цвет, используя ползунок
3) применяем операции закрытия, открытия для удаления шума и пустот
4) настраиваем маску
5) применяем маску к исходному изображению
'''

def nothing(x):
    pass

img = cv2.imread('classical_cv/img/M&Ms.jpg')
height = img.shape[0]
width = img.shape[1]
img = cv2.resize(img,(int(width/4),int(height/4)))

cv2.namedWindow('Image')
kernel = np.ones((5,5),np.uint8)

cv2.createTrackbar("H","Image",0,255,nothing)

while True:
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    
    h = cv2.getTrackbarPos("H",'Image')

    color_down = np.array([h-10, 100, 100])
    color_up = np.array([h+10, 255, 255])

    mask = cv2.inRange(hsv, color_down, color_up)
    opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    open_close = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    res = cv2.bitwise_and(img, img, mask=mask)
    res2 = cv2.bitwise_and(img, img, mask=opening)
    res3 = cv2.bitwise_and(img, img, mask=closing)
    res4 = cv2.bitwise_and(img, img, mask=open_close)

    cv2.imshow("Image", res)
    cv2.imshow("Image with opening", res2)
    cv2.imshow("Image with closing", res3)
    cv2.imshow("Image with open then close", res4)
    k = cv2.waitKey(2)
    if k == 27:
        cv2.destroyAllWindows()
        break