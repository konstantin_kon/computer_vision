import cv2
import numpy as np
'''
1) преобразовать видео в оттенки серого
2) сделать фильтрацию, чтобы убрать шум
3) выделить границы (Канни)
4) найти области интереса. задаются 3 вершины, получается треугольник, внутри которого находится дорожная разметка
'''


def findROI(img):
    height = img.shape[0]
    width = img.shape[1]
    region_of_interest_vertices = [(200,height), (width/2,height/1.37), (width-300,height)]
    roi = np.array([region_of_interest_vertices],np.int32)
    mask = np.zeros_like(img)
    match_mask_color = (255)
    cv2.fillPoly(mask, roi, match_mask_color)
    masked_img = cv2.bitwise_and(img,mask)
    #cv2.imshow("masked_img", masked_img)
    return masked_img

def findLine(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray,(5,5),0)
    canny = cv2.Canny(blur, 50, 100, apertureSize=3)
    img = findROI(canny)

    lines = cv2.HoughLinesP(img, 2, np.pi/180, threshold=50, minLineLength=10, maxLineGap=30)  
    return lines
    

def main():
    #cap = cv2.VideoCapture('/home/konstantin/cv/computer_vision/Part_1/img/Lane.mp4')
    cap = cv2.VideoCapture('./Part_1/img/Lane.mp4')
    while True:
        _, frame = cap.read()
        lines = findLine(frame)

        for line in lines:
            x1, y1, x2, y2 = line[0]
            cv2.line(frame,(x1,y1),(x2,y2),(255,0,0),2)
        cv2.imshow('Frame',frame)
        k = cv2.waitKey(1)
        if k == 27:
            break

    cv2.destroyAllWindows()
    cap.release()

if __name__ == '__main__':
    main()
