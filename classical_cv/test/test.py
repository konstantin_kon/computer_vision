from scipy.spatial import distance as dist
import numpy as np

a = np.array([(1,1)])
b = np.array([(3,2)])
D = dist.cdist(a,b)

def order_points(ptr):
    box = np.zeros((4,2))
    ptr = np.array(ptr)
    X = ptr[ptr[:,0].argsort()]
    left = X[:2,:]
    box[0] = left[0,:]
    box[3] = left[1,:]
    right = X[2:,:]

    dist1 = dist.cdist([box[0]], [right[0]])
    dist2 = dist.cdist([box[0]], [right[1]])
    if dist1 < dist2:
        box[1] = right[0]
        box[2] = right[1]
    else:
        box[2] = right[0]
        box[1] = right[1]

    return box

ptr = [[1,1], [3.5, 2], [1.5, 2.5], [3.5, 0.5]]
ptr2 = [[1.5, 2], [3.5, 0.5], [1,1], [3.5, 2]]

box = order_points(ptr2)
print('Result = ', box)
