import cv2

cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()
    cv2.imshow('Frame', frame)
    k = cv2.waitKey(1)
    if k == ord('s'):
        cv2.imwrite('/home/konstantin/cv/computer_vision/week1/img/object3.jpg', frame)
    elif k == 27:
        break

cv2.destroyAllWindows()
cap.release()