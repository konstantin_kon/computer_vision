'''
Попытка понять детектор углов Харриса
'''

import cv2
import numpy as np

img = cv2.imread('img/object3.jpg', 0)
'''
pt1 = (320,115)
pt2 = (340,135)
cv2.rectangle(img, pt1, pt2,(255,0,0))
cv2.circle(img, pt1,4,(0,255,0))
cv2.circle(img, pt2,4,(0,255,0))
'''
# выделение угла
frame1 = img[115:135,320:340] #[y1:y2,x1:x2]
frame2 = img[115:135,315:335]
cv2.imshow('Frame', frame1)
cv2.imshow('Image', img)

res = (frame1 - frame2)**2
print(res)
print('----------------')
print(frame1)
print('-----')
print(frame2)
cv2.imshow('Result', res)

# фон
bg1 = img[80:90,545:555] 
bg2 = img[80:90,543:553]
bg = (bg1 - bg2)**2
cv2.imshow("Bg", bg1)
cv2.imshow("Bg_result", bg)

# граница объекта
edge1 = img[135:165,465:480]
edge2 = img[137:167,467:482]
edge = (edge1 - edge2)**2
cv2.imshow("edge1", edge)

cv2.waitKey(0)
cv2.destroyAllWindows()