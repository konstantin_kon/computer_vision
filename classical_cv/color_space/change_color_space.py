import cv2

img = cv2.imread('week1/img/dog_bike.jpg')
hsv = cv2.cvtColor(img,cv2.COLOR_RGB2HSV)
h,s,v = cv2.split(hsv)
s1 = s*2
v = v*3
hsv = cv2.merge((h,s,v))
img_new = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

cv2.imshow('Image', img)
cv2.imshow('New image',img_new)
#cv2.imshow('Hue', h)
#cv2.imshow('Saturation', s)
#cv2.imshow('Value', v)

k = cv2.waitKey(0)
if k == 27:
    cv2.destroyAllWindows()